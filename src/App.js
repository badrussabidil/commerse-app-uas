import React,{useState,useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar
} from 'react-native';
import {DefaultTheme,Provider as PaperProvider} from 'react-native-paper';
// import 'react-native-gesture-handler';
import Route from './route/Route';


const theme = {
  ...DefaultTheme,
  roundness: 2,
  // version: 3,
  colors: {
    ...DefaultTheme.colors,
    primary: 'deepskyblue',
    // secondary: '#f1c40f',
    // tertiary: '#a1b2c3'rrrr
  },
};

const App = () => {
  return (
    <>
      <PaperProvider theme={theme}>
        <StatusBar barStyle="dark-content" backgroundColor="deepskyblue"/> 
        <View style={styles.container}>
          <Route/>
        </View>
      </PaperProvider>
    </>
  );
};

export default App;

const styles = StyleSheet.create({
container: {
  flex:1,
  // backgroundColor: '#fff',
},
});