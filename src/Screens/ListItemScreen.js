import React,{useEffect,useState} from 'react'
import { 
  View, 
  Text,
  FlatList,
  StyleSheet,
  Linking,
  Platform,
  
} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import { 
  Avatar, 
  Button, Card, 
  Title, 
  Paragraph 
} from 'react-native-paper';



const ListItemScreen = () => {

const myitems = [
  {
    name:"badrus",
    year:"2000",
    phone:"0987654321",
    image:"https://review1st.com/wp-content/uploads/2021/04/20210413_150427_copy_800x5283821790842931769407-768x507.jpeg",
    desc:"iphone tiga belas",
  },

  {
    name:"kamera",
    year:"2400",
    phone:"0987321",
    image:"https://www.tablety.pl/wp-content/uploads/2020/10/iphone_12-1-1-800x445.jpg",
    desc:"iphone tiga belas",
  },

]

const rendertem = (item) => {
  return (
      <Card style={styles.card}>
      <Card.Title title={item.name}/>
        <Card.Content>
          <Paragraph>{item.desc}</Paragraph>
          <Paragraph>{item.year}</Paragraph>
        </Card.Content>
        <Card.Cover source={{ uri: item.image }} />
        <Card.Actions>
          <Button>200</Button>
          <Button>call seller</Button>
      </Card.Actions>
    </Card>
    
  )
}

  return (
    <View>
      <FlatList
        data={myitems}
        keyExtractor={(item) => item.phone}
        renderItem={({item}) => rendertem(item)}
      />
      <FlatList
        data={myitems}
        keyExtractor={(item) => item.phone}
        renderItem={({item}) => rendertem(item)}
      />
    </View>
  );
};

export default ListItemScreen;

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'red',
  },
  card:{
    
    margin:10,
    elevation:2,
  },
});