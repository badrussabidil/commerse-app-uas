import React, {useState} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { TextInput,  } from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';

const CreateAdScreen = () => {

const [name,setName] = useState("")
const [desc,setDesc] = useState("")
const [year,setYear] = useState("")
const [price,setPrice] = useState("")
const [phone,setPhone] = useState("")

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Tambahkan Tawaran Baru</Text>

        <TextInput
          label='Judul'
          placeholder='tambahkan Judul'
          // value={'name'}
          mode='outlined'
          onChangeText={text => setName(text)}
        />

        <TextInput 
          label='Keterangan'
          placeholder='tambahkan keterangan'
          // value={'name'}
          mode='outlined'
          numberOfLines={2}
          multiline={true}
          onChangeText={text => setDesc(text)}
        />

        <TextInput 
          label='Tahun'
          placeholder='tambahkan tahun pembelian'
          // value={'name'}
          mode='outlined'
          keyboardType='numeric'
          onChangeText={text => setYear(text)}
        />

        <TextInput 
          label='Harga'
          placeholder='tambahkan harga INR'
          // value={'name'}
          mode='outlined'
          keyboardType='numeric'
          onChangeText={text => setPrice(text)}
        />

        <TextInput 
          label='Nomor'
          placeholder='tambahkan nomor kontak anda'
          // value={'name'}
          mode='outlined'
          keyboardType='numeric'
          onChangeText={text => setPhone(text)}
        />

        {/* tombol button */}
        <Button
            title='Upload Image'
            color="deepskyblue" 
            onPress={() => console.log('pressed')}
          />

          <Button
            title='Post'
            color="deepskyblue" 
            onPress={() => console.log('pressed')}
          />
    </View>
  );
};

export default CreateAdScreen;

const styles = StyleSheet.create({
  container: {
    flex:1,
    // margin:'5%',
    marginHorizontal:30,
    marginBottom:40,
    justifyContent:'space-evenly'
  },
  text:{
    fontSize:22,
    textAlign:"center",
    color: '#000'
},
});