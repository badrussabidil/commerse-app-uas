import React, {useEffect} from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Image ,
  StatusBar,
} from 'react-native';



const Splash = ({navigation}) => {
  useEffect(() =>{
    setTimeout(() => {
      navigation.replace('Beranda')
    },2000);
  });


  return (
    <View style={styles.container}>
      <View style={styles.wraper}>
        <Image source={require('../splashScreens/logo.png')} style={{width:200, height:200,}} />
      </View>
      <Text style={styles.textAtas}></Text>
      <Text style={styles.textBawah}>www.badrussabidil@gmail.com</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: 'deepskyblue',
  },
  textAtas: {
    fontWeight:'bold',
    fontSize:30,
    color:'#CDCDCD',
    marginBottom:8,
  },
  textBawah: {
    fontSize:20,
    color:'#fff',
    marginBottom:5,
  },
  wraper: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: 'deepskyblue',
  },
});
